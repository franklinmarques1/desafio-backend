# Desafio Backend Infomaniacs

O desafio consiste em criar uma API REST para um LMS que poderá ser consumida por um aplicativos Mobile ou Módulos WEB.

O candidato deve fazer um **fork** neste repositório e após o termino do desenvolvimento, realizar um **pull request** para análise do time.

O candidato tem a liberdade de realizar o desafio com a estruta que achar melhor, desde que utilize baseado em PHP, no caso de opção por uso de algum framework deverá ser optado por Laravel.
Deverá informar quais tecnologias foram usadas, como instalar, rodar e efetuar os acessos no arquivo `details.txt` (se necessário) para análise do desafio.

### Extra
- Utilizar Cache
- Rotas para exibição de frontend do CRUD
- Autenticação nas requisições
- Utilizar Docker

### POST `/lms/course`
Esse método deve receber um curso novo e inseri-lo em um banco de dados para ser consumido pela própria API.
```json
{
   "title":"Curso Programação Básica",
   "price":2500,
   "instructor":"João da Silva",
   "poster":"https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg",
   "date":"26/11/2017"
}
```
| Campo       | Tipo   |
|-------------|--------|
| id          | int    |
| title       | String |
| price       | int    |
| instructor  | String |
| poster      | String |
| date        | String |


### GET `/lms/courses`
Esse método da API deve retornar o seguinte JSON
```json
[
  {
    "title": "Curso Programação Básica",
    "price": 2500,
    "instructor": "João da Silva",
    "poster": "https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg",
    "date": "26/11/2017"
  },
  {
    "title": "Curso Programação Avançada",
    "price": 4500,
    "instructor": "João da Silva",
    "poster": "https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg",
    "date": "30/11/2017"
  },
  {
    "title": "Curso GIT",
    "price": 500,
    "instructor": "João da Silva",
    "poster": "https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg",
    "date": "30/11/2017"
  }
]
```

| Campo       | Tipo   |
|-------------|--------|
| title       | String |
| price       | int    |
| instructor  | String |
| poster      | String |
| date        | String |


Deve-se permitir ao usuário se matricular em um curso.
Para isso, você precisará fazer o método `buy` na sua API.

### POST `/lms/buy`
Esse método irá receber os dados da compra, junto com os dados do usuário.
```json
{
   "client_id":"7e655c6e-e8e5-4349-8348-e51e0ff3072e",
   "client_name":"Luke Skywalker",
   "course_id":"0001"
   "total_to_pay":1236,
   "credit_card":{
      "card_number":"1234123412341234",
      "value":7990,
      "cvv":789,
      "card_holder_name":"Luke Skywalker",
      "exp_date":"12/24"
   }
}

```

+ Transaction

| Campo        | Tipo       |
|--------------|------------|
| client_id    | String     |
| client_name  | String     |
| course_id    | int        |
| total_to_pay | int        |
| credit_card  | CreditCard |

+ CreditCard

| Campo            | Tipo   |
|------------------|--------|
| card_number      | String |
| card_holder_name | String |
| value            | int    |
| cvv              | int    |
| exp_date         | String |


### GET `/lms/history`
Esse método deve retornar todos as compras/matrículas realizadas na API
```json
[
   {
      "client_id":"7e655c6e-e8e5-4349-8348-e51e0ff3072e",
      "purchase_id":"569c30dc-6bdb-407a-b18b-3794f9b206a8",
      "course_title":"Curso Básico de Programação"
      "value":2500,
      "date":"19/08/2016",
      "card_number":"**** **** **** 1234"
   },
   {
      "client_id":"7e655c6e-e8e5-4349-8348-e51e0ff3072e",
      "purchase_id":"569c30dc-6bdb-407a-b18b-3794f9b206a8",
      "course_title":"Curso Básico de Programação"
      "value":2500,
      "date":"19/08/2016",
      "card_number":"**** **** **** 1234"
   },
   {
      "client_id":"7e655c6e-e8e5-4349-8348-e51e0ff3072e",
      "purchase_id":"569c30dc-6bdb-407a-b18b-3794f9b206a8",
      "course_title":"Curso Básico de Programação"
      "value":2500,
      "date":"19/08/2016",
      "card_number":"**** **** **** 1234"
   }
]
```
| Campo            | Tipo   |
|------------------|--------|
| card_number      | String |
| cliend_id        | String |
| course_title     | String |
| value            | int    |
| date             | String |
| purchase_id      | String |

### GET `/lms/history/{clientId}`
Esse método deve retornar todos as compras realizadas na API por um cliente específico
```json
[
   {
      "client_id":"7e655c6e-e8e5-4349-8348-e51e0ff3072e",
      "purchase_id":"569c30dc-6bdb-407a-b18b-3794f9b206a8",
      "course_title":"Curso Básico de Programação"
      "value":2500,
      "date":"19/08/2016",
      "card_number":"**** **** **** 1234"
   },
   {
      "client_id":"7e655c6e-e8e5-4349-8348-e51e0ff3072e",
      "purchase_id":"569c30dc-6bdb-407a-b18b-3794f9b206a8",
      "course_title":"Curso Básico de Programação"
      "value":2500,
      "date":"19/08/2016",
      "card_number":"**** **** **** 1234"
   }
]
```




---
#### LICENSE
```
MIT License

Copyright (c) 2017 Infomaniacs Ltda

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
