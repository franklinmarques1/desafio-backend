<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreditCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_cards')->insertOrIgnore([
            'card_number' => '1234123412341234',
            'value' => 7990,
            'cvv' => 789,
            'card_holder_name' => 'Luke Skywalker',
            'exp_date' => '12/24',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
