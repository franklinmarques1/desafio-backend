<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('histories')->insertOrIgnore([
            [
                'client_id' => '7e655c6e-e8e5-4349-8348-e51e0ff3072e',
                'purchase_id' => '569c30dc-6bdb-407a-b18b-3794f9b206a8',
                'course_id' => 1,
                'value' => 2500,
                'date' => '2016-08-19',
                'card_number' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'client_id' => '7e655c6e-e8e5-4349-8348-e51e0ff3072e',
                'purchase_id' => '569c30dc-6bdb-407a-b18b-3794f9b206a8',
                'course_id' => 1,
                'value' => 2500,
                'date' => '2016-08-19',
                'card_number' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'client_id' => '7e655c6e-e8e5-4349-8348-e51e0ff3072e',
                'purchase_id' => '569c30dc-6bdb-407a-b18b-3794f9b206a8',
                'course_id' => 1,
                'value' => 2500,
                'date' => '2016-08-19',
                'card_number' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
}
