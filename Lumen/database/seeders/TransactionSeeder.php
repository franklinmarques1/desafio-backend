<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->insertOrIgnore([
            'client_id' => '7e655c6e-e8e5-4349-8348-e51e0ff3072e',
            'client_name' => 'Luke Skywalker',
            'course_id' => 1,
            'total_to_pay' => 1236,
            'credit_card' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
