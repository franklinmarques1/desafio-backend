<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'lms'], function ($router) {
    $router->post('course', 'CourseController@create');
    $router->get('courses', 'CourseController@index');
    $router->post('buy', 'BuyController@create');
    $router->get('history', 'HistoryController@index');
    $router->get('history/{clientId}', 'HistoryController@show');
});

$router->get('/', function () use ($router) {
    return $router->app->version();
});
