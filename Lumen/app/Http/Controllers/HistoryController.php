<?php

namespace App\Http\Controllers;

use App\Models\History;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class HistoryController extends Controller
{
    public function index(): JsonResponse
    {
        return Cache::remember('history', 60, function () {
            return response()->json(History::all());
        });
    }

    public function show($clientId): JsonResponse
    {
        return Cache::remember('history_' . $clientId, 60, function () use ($clientId) {
            return response()->json(History::where('client_id', $clientId)->get());
        });
    }
}
