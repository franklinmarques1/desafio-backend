<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class BuyController extends Controller
{
    public function create(Request $request): JsonResponse
    {
        $validator = $this->validateInputData($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->messages(), 400);
        }

        $transaction = new Transaction();
        $transaction->client_id = $request->input('client_id');
        $transaction->client_name = $request->input('client_name');
        $transaction->course_id = $request->input('course_id');
        $transaction->total_to_pay = $request->input('total_to_pay');
        $transaction->credit_card = $request->input('credit_card');
        $transaction->save();

        Cache::flush();

        return response()->json([
            'message' => "Compra adicionada com sucesso."
        ], 201);
    }

    private function validateInputData($input): \Illuminate\Contracts\Validation\Validator
    {
        $rules = [
            'client_id' => 'required|max:255',
            'client_name' => 'required|max:255',
            'course_id' => 'required|max:255',
            'total_to_pay' => 'required|integer|digits_between:1,11',
            'credit_card' => 'required|max:19',
        ];

        $messages = [
            'required' => 'O :attribute é requerido.',
            'max' => 'O :attribute deve possuir até :max caracteres.',
            'integer' => 'O :attribute deve possuir valor numérico.',
            'digits_between' => 'O :attribute deve conter de :min a :max dígitos.',
        ];

        return Validator::make($input, $rules, $messages, [
            'client_id' => 'código do cliente',
            'client_name' => 'nome do cliente',
            'course_id' => 'curso',
            'total_to_pay' => 'total a pagar',
            'credit_card' => 'cartão de crédito',
        ]);
    }

}
