<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function index(): JsonResponse
    {
        return Cache::remember('courses', 60, function () {
            return response()->json(Course::all());
        });
    }

    public function create(Request $request): JsonResponse
    {
        $validator = $this->validateInputData($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->messages(), 400);
        }

        $course = new Course();
        $course->title = $request->input('title');
        $course->price = $request->input('price');
        $course->instructor = $request->input('instructor');
        $course->poster = $request->input('poster');
        $course->date = $request->input('date');
        $course->save();

        Cache::forget('courses');

        return response()->json([
            'message' => "Curso adicionado com sucesso."
        ], 201);
    }

    private function validateInputData($input): \Illuminate\Contracts\Validation\Validator
    {
        $rules = [
            'title' => 'required|max:255',
            'price' => 'required|integer|digits_between:1,11',
            'instructor' => 'required|max:255',
            'poster' => 'required|max:255',
            'date' => 'required|date_format:d/m/Y',
        ];

        $messages = [
            'required' => ':attribute é requerido.',
            'max' => ':attribute deve possuir até :max caracteres.',
            'integer' => ':attribute deve possuir valor numérico.',
            'digits_between' => ':attribute deve conter de :min a :max dígitos.',
            'date_format' => ':attribute deve possuir um formato válido.',
        ];

        return Validator::make($input, $rules, $messages, [
            'title' => 'O título',
            'price' => 'O preço',
            'instructor' => 'O(A) instrutor(a)',
            'poster' => 'O anúncio',
            'date' => 'A data',
        ]);
    }

}
