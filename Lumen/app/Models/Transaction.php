<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Transaction extends Model
{
    protected $fillable = [
        'client_id', 'client_name', 'course_id', 'total_to_pay', 'credit_card'
    ];
    protected $hidden = ['id', 'created_at', 'updated_at'];

    public function getCourseIdAttribute($courseId)
    {
        return Course::where('id', '=', $courseId)->first()->title;
    }

    public function setCourseIdAttribute($courseTitle)
    {
        $this->attributes['course_id'] =  Course::where('title', '=', $courseTitle)->first()->id;
    }

    public function getCreditCardAttribute($creditCard)
    {
        return CreditCard::where('id', '=', $creditCard)->first()->card_number;
    }

    public function setCreditCardAttribute($cardNumber)
    {
        $this->attributes['credit_card'] =  CreditCard::where('card_number', '=', $cardNumber)->first()->id;
    }

    public function course(): HasOne
    {
        return $this->hasOne(Course::class, 'course_id');
    }

    public function creditCard(): HasOne
    {
        return $this->hasOne(CreditCard::class, 'credit_card');
    }

}
