<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Course extends Model
{
    protected $fillable = [
        'title', 'price', 'instructor', 'poster', 'date'
    ];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    public function getDateAttribute($date)
    {
        return date('d/m/Y', strtotime($date));
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = date('Y-m-d', strtotime(str_replace('/', '-', $date)));
    }

    public function transaction(): HasMany
    {
        return $this->hasMany(Transaction::class, 'course_id');
    }

    public function history(): HasMany
    {
        return $this->hasMany(History::class, 'course_id');
    }

}
