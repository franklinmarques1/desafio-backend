<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CreditCard extends Model
{
    protected $fillable = [
        'card_number', 'value', 'cvv', 'card_holder_name', 'exp_date'
    ];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    public function course(): BelongsToMany
    {
        return $this->belongsToMany(Transaction::class, 'credit_card');
    }

}
