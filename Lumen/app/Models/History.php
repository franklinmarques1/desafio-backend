<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class History extends Model
{
    protected $fillable = [
        'client_id', 'purchase_id', 'course_id', 'value', 'date', 'card_number'
    ];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    public function getCourseIdAttribute($courseId)
    {
        return Course::where('id', '=', $courseId)->first()->title;
    }

    public function setCourseIdAttribute($courseTitle)
    {
        $this->attributes['course_id'] =  Course::where('title', '=', $courseTitle)->first()->id;
    }

    public function getDateAttribute($date)
    {
        return date('d/m/Y', strtotime($date));
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] =  date('Y-m-d', strtotime(str_replace('/', '-', $date)));
    }

    public function getCardNumberAttribute($cardNumber)
    {
        $creditCard = CreditCard::where('id', $cardNumber)->first();
        return '**** **** **** ' . substr($creditCard->card_number, -4);
    }

    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

}
