-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 12-Ago-2021 às 10:31
-- Versão do servidor: 5.7.33
-- versão do PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `desafio_backend`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `instructor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poster` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `courses`
--

INSERT INTO `courses` (`id`, `title`, `price`, `instructor`, `poster`, `date`, `created_at`, `updated_at`) VALUES
(1, 'Curso Programação Básica', 2500, 'João da Silva', 'https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg', '2017-11-26', '2021-08-12 09:53:43', '2021-08-12 09:53:43'),
(2, 'Curso Programação Avançada', 4500, 'João da Silva', 'https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg', '2017-11-30', '2021-08-12 09:53:43', '2021-08-12 09:53:43'),
(3, 'Curso GIT', 500, 'João da Silva', 'https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg', '2017-11-30', '2021-08-12 09:53:43', '2021-08-12 09:53:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `credit_cards`
--

CREATE TABLE `credit_cards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_holder_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `cvv` int(11) NOT NULL,
  `exp_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `credit_cards`
--

INSERT INTO `credit_cards` (`id`, `card_number`, `card_holder_name`, `value`, `cvv`, `exp_date`, `created_at`, `updated_at`) VALUES
(1, '1234123412341234', 'Luke Skywalker', 7990, 789, '12/24', '2021-08-12 09:53:43', '2021-08-12 09:53:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `histories`
--

CREATE TABLE `histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchase_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `value` int(11) NOT NULL,
  `date` date NOT NULL,
  `card_number` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `histories`
--

INSERT INTO `histories` (`id`, `client_id`, `purchase_id`, `course_id`, `value`, `date`, `card_number`, `created_at`, `updated_at`) VALUES
(1, '7e655c6e-e8e5-4349-8348-e51e0ff3072e', '569c30dc-6bdb-407a-b18b-3794f9b206a8', 1, 2500, '2016-08-19', 1, '2021-08-12 09:53:43', '2021-08-12 09:53:43'),
(2, '7e655c6e-e8e5-4349-8348-e51e0ff3072e', '569c30dc-6bdb-407a-b18b-3794f9b206a8', 1, 2500, '2016-08-19', 1, '2021-08-12 09:53:43', '2021-08-12 09:53:43'),
(3, '7e655c6e-e8e5-4349-8348-e51e0ff3072e', '569c30dc-6bdb-407a-b18b-3794f9b206a8', 1, 2500, '2016-08-19', 1, '2021-08-12 09:53:43', '2021-08-12 09:53:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_08_11_143458_create_courses_table', 1),
(2, '2021_08_11_143605_create_credit_cards_table', 1),
(3, '2021_08_11_143730_create_histories_table', 1),
(4, '2021_08_11_144542_create_transactions_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `total_to_pay` int(11) NOT NULL,
  `credit_card` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `transactions`
--

INSERT INTO `transactions` (`id`, `client_id`, `client_name`, `course_id`, `total_to_pay`, `credit_card`, `created_at`, `updated_at`) VALUES
(1, '7e655c6e-e8e5-4349-8348-e51e0ff3072e', 'Luke Skywalker', 1, 1236, 1, '2021-08-12 09:53:43', '2021-08-12 09:53:43');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `credit_cards`
--
ALTER TABLE `credit_cards`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `histories_course_id_foreign` (`course_id`),
  ADD KEY `histories_card_number_foreign` (`card_number`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_course_id_foreign` (`course_id`),
  ADD KEY `transactions_credit_card_foreign` (`credit_card`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `credit_cards`
--
ALTER TABLE `credit_cards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `histories`
--
ALTER TABLE `histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `histories`
--
ALTER TABLE `histories`
  ADD CONSTRAINT `histories_card_number_foreign` FOREIGN KEY (`card_number`) REFERENCES `credit_cards` (`id`),
  ADD CONSTRAINT `histories_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Limitadores para a tabela `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `transactions_credit_card_foreign` FOREIGN KEY (`credit_card`) REFERENCES `credit_cards` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
